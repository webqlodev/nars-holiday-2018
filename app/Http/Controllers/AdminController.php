<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use Excel;
use Datatables;
use View;
use App\Mail\ThankYouForRegistering;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Carbon\Carbon;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $start = DB::table('campaign')->where('key', 'start_time')->value('value');
        $end = DB::table('campaign')->where('key', 'end_time')->value('value');
        $campaign_time['start'] = $start;
        $campaign_time['end'] = $end;
        return View::make('dashboard')->with('campaign_time', $campaign_time);
        // return view('dashboard');
    }

    /**
     * Save campaign date range
     *
     * @return response
     */
    public function saveDate(Request $request)
    {
        DB::table('campaign')
            ->where('key', 'start_time')
            ->update(['value' => $request->start]);
        DB::table('campaign')
            ->where('key', 'end_time')
            ->update(['value' => $request->end]);
        $response = array(
            'status' => 'success',
            'msg' => 'Date saved.',
            'start' => $request->start,
            'end' => $request->end,
        );
        return response()->json($response);
        // return redirect()->route('admin-dashboard');
    }

    public function generateUniqueCodes($amount = 500)
    {
        $allowedChars = '1234567890';

        for ($i = 0; $i < $amount; $i++) {
            $code = '';

            for ($j = 0; $j < 5; $j++) {
                $code .= $allowedChars[mt_rand(0, strlen($allowedChars) - 1)];
            }

            $existed = DB::table('codes')->where('code', $code)->count();

            if (!$existed) {
                DB::table('codes')->insert([
                    'code' => $code,
                    'used' => false,
                ]);
            }
        }

        return $amount . ' codes generated <button onclick="window.close()">Close</button>';
    }

    public function DatatablesTotalRegistration()
    {
        $total = new Collection;
        // $startDate = new Carbon( env('START_TIME') );
        // $endDate = new Carbon( env('END_TIME') );
        $start = DB::table('campaign')->where('key', 'start_time')->value('value');
        $end = DB::table('campaign')->where('key', 'end_time')->value('value');
        $startDate = new Carbon( $start );
        $endDate = new Carbon( $end );

        // Loop through registrations table by date

        $date = $startDate;

        while ( $date->lte($endDate) && !$date->isTomorrow() ) {
            $total->push([
                'date' => $date->format('j F Y (l)'),
                'total' => DB::table('registrations')->whereBetween( 'created_at', [
                    $date->toDateString(),
                    $date->addDay()->toDateString(),
                ])->count(),
            ]);
        }

        $total->reverse();

        // Reorder to descending order
        $total = $total->reverse();

        return Datatables::of($total)->make(true);
    }

    public function DatatablesRegistrationList()
    {
        $registrations = DB::table('registrations')->select([
            'id',
            'email',
            'unique_code as code',
            'created_at as time',
        ])->orderBy('created_at', 'desc')->get();

        $registrations->transform(function ($item, $key) {
            $time = new Carbon($item->time);
            $item->time = $time->format('j F Y (l) h:i a');
            return $item;
        });

        return Datatables::of($registrations)->make(true);
    }

    public function resendEmail($code)
    {
        
        HomeController::makeVoucher($code);
        Mail::to( DB::table('registrations')->where('unique_code', $code)->value('email') )->send( new ThankYouForRegistering($code) );

        return 'Email sent. <button onclick="window.close()">Close</button>';
    }

    public function exportRegistrationList()
    {
        $fileName = env('APP_NAME') . ' @ ' . Carbon::now();

        Excel::create($fileName, function ($excel) {
            // left align all rows

            $excel->getDefaultStyle()
                ->getAlignment()
                ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $excel->sheet('registrations', function ($sheet) {
                $rowIndex = 1;

                $sheet->row($rowIndex, [
                    'Email Address',
                    'Redeem Code',
                    'Register Time',
                ]);

                $registrations = DB::table('registrations')->orderBy('id', 'asc')->get();

                foreach ($registrations as $key => $value) {
                    $rowIndex++;
                    $time = new Carbon($value->created_at);

                    $sheet->row($rowIndex, [
                        $value->email,
                        $value->unique_code,
                        $time->format('j F Y (l) h:i a'),
                    ]);
                }

                $sheet->setAutoSize(true);
            });
        })->download('xlsx');
    }

    public function exportCodeList()
    {
        $fileName = env('APP_NAME') . ' @ ' . Carbon::now() . ' (Code list)';

        Excel::create($fileName, function ($excel) {
            // left align all rows

            $excel->getDefaultStyle()
                ->getAlignment()
                ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $excel->sheet('codes', function ($sheet) {
                $rowIndex = 1;

                $sheet->row($rowIndex, [
                    'No.',
                    'Code',
                ]);

                $codes = DB::table('codes')->orderBy('id', 'asc')->get();

                foreach ($codes as $key => $value) {
                    $rowIndex++;

                    $sheet->row($rowIndex, [
                        $value->id,
                        $value->code,
                    ]);
                }

                $sheet->setAutoSize(true);
            });
        })->download('xlsx');
    }
}
