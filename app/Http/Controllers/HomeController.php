<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use Validator;
use App\Mail\ThankYouForRegistering;
use Illuminate\Http\Request;
use Carbon\Carbon;
//use Spatie\Browsershot\Browsershot; //required php7.1


class HomeController extends Controller
{
    /**
     * Show the application home.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $end = DB::table('campaign')->where('key', 'end_time')->value('value');
        // $start = DB::table('campaign')->where('key', 'start_time')->value('value');
        // if ( Carbon::parse( env('END_TIME') )->isFuture() ) {
        if ( Carbon::parse($end)->isFuture() ) {
            return view('form');
        } else {
            return view('end', ['end'=>$end] );
        }
    }

    public function submit(Request $request)
    {
        $validator = Validator::make($request->all(), [
    		'email' => 'required|email|max:255|unique:registrations,email',
        ], [
            'email.required' => 'Email address is required to participate.',
            'email.email' => 'Email address must be valid.',
            'email.max' => 'Email address is too long.',
            'email.unique' => 'This email address has already been registered.',
        ]);

        if ( $validator->fails() ) {
            return back()->withErrors($validator)->withInput();
        }

        // Get code from code table

        $codeObj = DB::table('codes')->where('used', false)->first();
        if ($codeObj){
            $code = $codeObj->code;
            DB::table('codes')->where('code', $code)->update(['used' => true]);
        }
        else{ // handle when all code used.
            $code = $this->generateUniqueCode();
            DB::table('codes')->insert([
                'code' => $code,
                'used' => true,
            ]);
        }

        // Create new registration

        DB::table('registrations')->insert([
            'email' => $request->email,
            'unique_code' => $code,
            'created_at' => Carbon::now(),
        ]);

        // Send email
        HomeController::makeVoucher($code);
        Mail::to( $request->email )->send( new ThankYouForRegistering($code) );

        return redirect()->route('thank-you', ['code' => $code]);
    }

    public function thankYou($code)
    {
        $registration = DB::table('registrations')->where('unique_code', $code)->first();

        if ($registration) {
            return view('tq', [
                'code' => $code,
            ]);
        } else {
            abort(404);
        }
    }

    // public function thankYou()
    // {
    //     return view('tq');
    // }

    public function redirect($target)
    {
        DB::table('clicks')->insert([
            'target' => $target,
            'created_at' => Carbon::now(),
        ]);

        switch ($target) {
            case 'facebook':
            case 'facebook-email':
                $url = 'https://www.facebook.com/MyHeycha/';
                break;
        }

        return redirect($url);
    }

    public function generateUniqueCode()
    {
        $allowedChars = '1234567890';

        while (true) {
            $code = '';

            for ($i = 0; $i < 5; $i++) {
                $code .= $allowedChars[mt_rand(0, strlen($allowedChars) - 1)];
            }

            $existed = DB::table('registrations')->where('unique_code', $code)->count();

            if (!$existed) {
                break;
            }
        }

         return $code;
    }

    public static function makeVoucher($code) // image method.
    {
        $im = imagecreatefromjpeg( public_path('holiday-voucher-template-latest-3.jpg') );

        imagettftext($im, 30, 0, 638, 1611, imagecolorallocate($im, 255, 255, 255), public_path('fonts/HelveticaNeueLTStd-Md.otf'), 'CM'.$code.'.');

        return imagejpeg($im, public_path('voucher/') . $code . '.jpg');
    }

    public function testEmail() // email method, email screenshot method.
    {
        $email = ['novadean90@gmail.com'];
        $code = '00001';
        $user = DB::table('registrations')->where('unique_code', $code)->first();
        // Mail::to( $email )->send( new ThankYouForRegistering($code) );

        // Browsershot::html(view('email.thank-you',['user'=>$user])->render())->windowSize(600, 600)->setScreenshotType('jpeg', 90)->fullPage()->save('voucher/'.$code.'.png'); //Screenshot
        HomeController::makeVoucher($code);
        Mail::to( $email )->send( new ThankYouForRegistering($code) );
        return view('email.voucher',['user'=>$user]);
        //return redirect()->route('thank-you', ['code' => $code]);
    }
}
