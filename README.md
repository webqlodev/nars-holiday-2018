1.) Copy and Modify .env
```
cp .env.example .env
```
2.) Run Composer install
```
composer install
```

3.)Generate Key
```
php artisan key:generate
```

4.)Migrate Database
```
php artisan migrate
```
