<!doctype html>
<html>
<head>
    <!--[if gte mso 7]><xml>
      <o:OfficeDocumentSettings>
      <o:AllowPNG/>
      <o:PixelsPerInch>96</o:PixelsPerInch>
      </o:OfficeDocumentSettings>
    </xml><![endif]-->
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name=”x-apple-disable-message-reformatting”>
    <title>Thank You for Participating</title>
    <style>
        /* -------------------------------------
        GLOBAL
        ------------------------------------- */
        @font-face {
            font-family: "Helvetica Neue Light";
            src: url("/fonts/HelveticaNeueLTStd-Lt.otf");
        }

        @font-face {
            font-family: "Helvetica Neue Thin";
            src: url("/fonts/HelveticaNeueLTStd-Th.otf");
        }

        @font-face {
            font-family: "Helvetica Neue Ult Light";
            src: url("/fonts/HelveticaNeueLTStd-UltLt.otf");
        }

        @font-face {
            font-family: "Helvetica Neue Medium";
            src: url("/fonts/HelveticaNeue-Medium.otf");
        }

        * {
            font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
            font-size: 100%;
            line-height: 1;
            margin: 0;
            padding: 0;
            /*font-weight: 500;*/
        }

        img {
            max-width: 600px;
            width: auto;
        }
        html{
            width: 100%;
            margin:0 auto;
        }
        body {
            -webkit-font-smoothing: antialiased;
            height: 100%;
            -webkit-text-size-adjust: none;
            /*width: 600px !important;*/
            margin:0 auto;
            width:100%;
            /*width:600px;*/
        }

        /* -------------------------------------
        ELEMENTS
        ------------------------------------- */
        a {
            color: #348eda;
        }

        .btn-primary {
            Margin-bottom: 10px;
            width: auto !important;
        }

        .btn-primary td {
            background-color: #348eda;
            border-radius: 25px;
            font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
            font-size: 14px;
            text-align: center;
            vertical-align: top;
        }

        .btn-primary td a {
            background-color: #348eda;
            border: solid 1px #348eda;
            border-radius: 25px;
            border-width: 10px 20px;
            display: inline-block;
            color: #ffffff;
            cursor: pointer;
            font-weight: bold;
            line-height: 2;
            text-decoration: none;
        }

        .last {
            margin-bottom: 0;
        }

        .first {
            margin-top: 0;
        }

        .padding {
            padding: 10px 0;
        }


        /* -------------------------------------
        BODY
        ------------------------------------- */
        table.body-wrap {
            width: 100%;
            margin:0 auto;
        }

        table.body-wrap .container {
            /*border: 1px solid #f0f0f0;*/
            border:0;
        }


        /* -------------------------------------
        FOOTER
        ------------------------------------- */
        table.footer-wrap {
            clear: both !important;
            width: 100%;
            margin:0 auto;
        }

        .footer-wrap .container p {
            color: #666666;
            font-size: 12px;

        }

        table.footer-wrap a {
            color: #999999;
        }


        /* -------------------------------------
        TYPOGRAPHY
        ------------------------------------- */
        h1,
        h2,
        h3 {
            color: #111111;
            font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
            font-weight: 200;
            line-height: 1.2em;
            margin: 40px 0 10px;
        }

        h1 {
            font-size: 36px;
        }
        h2 {
            font-size: 28px;
        }
        h3 {
            font-size: 22px;
        }

        p,
        ul,
        ol {
            font-size: 14px;
            font-weight: 200;
            margin-bottom: 10px;
        }

        ul li,
        ol li {
            margin-left: 5px;
            list-style-position: inside;
        }

        /* ---------------------------------------------------
        RESPONSIVENESS
        ------------------------------------------------------ */

        /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
        .container {
            clear: both !important;
            display: block !important;
            margin: 0 auto !important;
            max-width: 600px !important;
        }

        /* Set the padding on the td rather than the div for Outlook compatibility */
        .body-wrap .container {
            /* padding: 20px; */
        }

        /* This should also be a block element, so that it will fill 100% of the .container */
        .content {
            display: block;
            margin: 0 auto;
            width:100%;
            width:600px;
            max-width: 600px;
            text-align: center;
        }

        /* Let's make sure tables in the content area are 100% wide */
        .content table {
            width: 100%;
        }
        .code {
            font-size: 20px;
        }
        @media (max-width:768px) and (min-width:415px){
            .code{
                font-size: 20px;
            }
        }
        @media (max-width:414px) and (min-width:376px){
            .code{
                font-size: 5px;
            }
        }
        @media (max-width:375px) and (min-width:361px){
            .code{
                font-size: 5px;
            }
        }
        @media (max-width:360px) and (min-width:321px){
            .code{
                font-size: 3px;
            }
        }
        @media (max-width:320px){
            .code{
                font-size: 2px;
            }
        }
        
    </style>
</head>

<body bgcolor="#f6f6f6" style="padding:0; margin:0 auto; text-align:center">
    <!-- bgcolor="#f6f6f6"  -->
<!--     <table class="body-wrap" bgcolor="#f6f6f6">
        <tr>
            <td></td>
            <td class="container" bgcolor="#FFFFFF">
                -->
                <div class="content" style="margin:0 auto"> 
                    @yield('content')
                </div>
            <!--</td>
            <td></td>
        </tr>
    </table> -->
</body>
</html>
