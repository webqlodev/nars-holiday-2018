@extends('layouts.app')

@push('css')
    <style>
        /* -------------------------------------
        GLOBAL
        ------------------------------------- */
        * {
            font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
            font-size: 100%;
            line-height: 1;
            margin: 0;
            padding: 0;
        }

        img {
            max-width: 600px;
            width: auto;
        }

        body {
            -webkit-font-smoothing: antialiased;
            height: 100%;
            -webkit-text-size-adjust: none;
            width: 100% !important;
            /*background-color: #000;*/
            background-color: #fff;
        }


        /* -------------------------------------
        ELEMENTS
        ------------------------------------- */
        a {
            color: #348eda;
        }

        .btn-primary {
            Margin-bottom: 10px;
            width: auto !important;
        }

        .btn-primary td {
            background-color: #348eda;
            border-radius: 25px;
            font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
            font-size: 14px;
            text-align: center;
            vertical-align: top;
        }

        .btn-primary td a {
            background-color: #348eda;
            border: solid 1px #348eda;
            border-radius: 25px;
            border-width: 10px 20px;
            display: inline-block;
            color: #ffffff;
            cursor: pointer;
            font-weight: bold;
            line-height: 2;
            text-decoration: none;
        }

        .last {
            margin-bottom: 0;
        }

        .first {
            margin-top: 0;
        }

        .padding {
            padding: 10px 0;
        }


        /* -------------------------------------
        BODY
        ------------------------------------- */
        table.body-wrap {
            width: 100%;
        }

        table.body-wrap .container {
            border: 1px solid #f0f0f0;
        }


        /* -------------------------------------
        FOOTER
        ------------------------------------- */
        table.footer-wrap {
            clear: both !important;
            width: 100%;
        }

        .footer-wrap .container p {
            color: #666666;
            font-size: 12px;

        }

        table.footer-wrap a {
            color: #999999;
        }


        /* -------------------------------------
        TYPOGRAPHY
        ------------------------------------- */
        h1,
        h2,
        h3 {
            color: #111111;
            font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
            font-weight: 200;
            line-height: 1.2em;
            margin: 40px 0 10px;
        }

        h1 {
            font-size: 36px;
        }
        h2 {
            font-size: 28px;
        }
        h3 {
            font-size: 22px;
        }

        p,
        ul,
        ol {
            font-size: 14px;
            font-weight: 200;
            margin-bottom: 10px;
        }

        ul li,
        ol li {
            margin-left: 5px;
            list-style-position: inside;
        }

        /* ---------------------------------------------------
        RESPONSIVENESS
        ------------------------------------------------------ */

        /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
        .container {
            clear: both !important;
            display: block !important;
            Margin: 0 auto !important;
            max-width: 600px !important;
            width: 100%;
        }

        /* Set the padding on the td rather than the div for Outlook compatibility */
        .body-wrap .container {
            /* padding: 20px; */
        }

        /* This should also be a block element, so that it will fill 100% of the .container */
        .content {
            display: block;
            margin: 0 auto;
            max-width: 600px;
            width: 100%;
        }

        /* Let's make sure tables in the content area are 100% wide */
        .content table {
            width: 100%;
        }

        #app table {
            margin: 0 auto;
            background-color: #fff;
            color: #000;
        }
    </style>
@endpush

@section('content')
    <table>
        <tr>
            <td style="font-size: 0;">
                <img alt="NARS" src="{{ asset('images/nars/nars_climax_email_header.gif') }}" style="width:100%;" />
            </td>
        </tr>
        <tr>
            <td style="font-size: 0; position: relative;">
                <img src="{{ asset('images/nars/nars_climax_email_body.gif') }}" style="width: 100%;">
               {{--  <div style="position: absolute; width: 40%; right: 2%; top: 20%;">
                    <h1 style="font-size: 20px; line-height: 1.2; text-align: right; font-weight: lighter; margin-bottom: 0; text-transform: uppercase;">
                        Patent Shine.
                        <br />
                        High-Octane
                        <br />
                        Color.
                    </h1>
                    <h4 style="font-size: 12px; line-height: 1.2; text-align: right;font-weight: lighter; margin-top: 0;">
                        Introducing Full
                        <br />
                        Vinyl Lip Lacquer.
                    </h4>
                </div>
                <video id="video" width="600" height="600" controls playsinline muted loop autoplay style="max-width:600px;">
                @if (Carbon\Carbon::today('Asia/Singapore') < Carbon\Carbon::create(2018, 7, 9, 0, 0, 0))
                    <source src="{{ asset('images/nars/video/01_NARS_LIP_REDDISTRICT.mp4') }}" type="video/mp4">
                @elseif (Carbon\Carbon::today('Asia/Singapore') > Carbon\Carbon::create(2018, 7, 8, 0, 0, 0) && Carbon\Carbon::today('Asia/Singapore') < Carbon\Carbon::create(2018, 7, 16, 0, 0, 0))
                    <source src="{{ asset('images/nars/video/02_NARS_LIP_SANTO.mp4') }}" type="video/mp4">
                @elseif (Carbon\Carbon::today('Asia/Singapore') > Carbon\Carbon::create(2018, 7, 15, 0, 0, 0) && Carbon\Carbon::today('Asia/Singapore') < Carbon\Carbon::create(2018, 7, 23, 0, 0, 0))
                    <source src="{{ asset('images/nars/video/03_NARS_LIP_Abruzzo.mp4') }}" type="video/mp4">
                @elseif (Carbon\Carbon::today('Asia/Singapore') > Carbon\Carbon::create(2018, 7, 22, 0, 0, 0) && Carbon\Carbon::today('Asia/Singapore') < Carbon\Carbon::create(2018, 7, 30, 0, 0, 0))
                    <source src="{{ asset('images/nars/video/05_NARS_LIP_Valencia.mp4') }}" type="video/mp4">
                @else
                    <source src="{{ asset('images/nars/video/06_NARS_LIP_Baden.mp4') }}" type="video/mp4">
                @endif
                </video> --}}
            </td>
        </tr>
        <tr>
            {{-- <td style="background-color: #000; text-align: center; color: #fff;">
                <p style="margin-top: 12px; margin-bottom: 6px; font-size: 24px;">
                    Thank You!
                </p>

                <p style="padding: 0px 10px; margin-bottom: 18px; font-size: 16px;">
                    We have sent you an email with the redemption code.
                    <br />
                    Kindly check spam folder if you could not find it in inbox.
                </p>
            </td> --}}
            <td style="background-color: #000; text-align: center; color: #fff;">
                <img src="{{ asset('images/nars/hashtag.png') }}" style="width: 48%;margin: 30px 0 0;">

                <p style="margin-bottom: 6px; font-size: 18px;">
                    Present this code to enjoy an ultimate eye makeover
                    <br />
                    and receive a deluxe-size gift.*
                </p>

                <p style="margin: 25px 0; font-size: 14px;font-weight: bold">
                    {{$code}}
                </p>

                <p style="margin-bottom: 30px; font-size: 9.5px;font-family:'Helvetica Neue LT W01_55 Roman'">
                    *Limited quantities available. Items may vary at different stores. NARS reserves the right to modify or amend the validity of this offer <br />
                    without prior notice. Other terms and conditions apply. NARS Cosmetics is available at Pavilion KL, Suria KLCC, Mid Valley <br />
                    Megamall, Sunway Pyramid, AEON Tebrau City, Parkson Gurney and Sky Avenue Genting.
                </p>
            </td>
        </tr>
        {{-- <tr>
            <td style="text-align: center;">
                <p style="padding: 0px 10px;margin-top: 12px; font-size: 13px;">
                    NARS COSMETICS IS AVAILABLE AT PAVILION KL, SURIA KLCC,
                    <br />
                    MID VALLEY MEGAMALL, SUNWAY PYRAMID, AEON TEBRAU CITY,
                    <br />
                    PARKSON GURNEY AND SKY AVENUE GENTING.
                </p>

                <p style="font-size: 11px; width: 100%; max-width: 600px; padding-left: 20px; padding-right: 20px;">
                    Limited quantities available. Items may vary at different stores. NARS reserves the right to modify or amend the validity of this offer without prior notice. Other terms and conditions apply.
                </p>
            </td>
        </tr> --}}
    </table>
@endsection
